import mlcontour from "maplibre-contour/dist/index.mjs";
import {
  transpoLabelLayers,
  tunnelTranspoLayers,
  bridgeTranspoLayers,
  surfaceTranspoLayers,
} from "./layers/transpo.js";
import { naturalEarthShadedReliefLayer } from "./layers/naturalearth.js";
import { waterLayers, waterLabelLayers } from "./layers/water.js";
import * as landcoverLayers from "./layers/landcover.js";
import * as landuseLayers from "./layers/landuse.js";
import { contourLayers } from "./layers/contours.js";
import * as poiLayers from "./layers/poi.js";
import * as physicalLayers from "./layers/physical.js";
import { hillshadeLayer } from "./layers/hillshade.js";
import { boundariesLayers } from "./layers/boundaries.js";
import { placesLayers } from "./layers/places.js";
import buildingsLayer from "./layers/buildings.js";

export class TrailStash {
  constructor({ root, tileUrl }) {
    this.root = root;
    this.tileUrl = tileUrl;
  }
  setupMapLibreGL(maplibregl) {
    if (!this.demSource) {
      this.demSource = new mlcontour.DemSource({
        url: "https://s3.amazonaws.com/elevation-tiles-prod/terrarium/{z}/{x}/{y}.png",
        encoding: "terrarium",
        maxzoom: 15,
        worker: true,
        cacheSize: 100,
        timeoutMs: 10_000,
      });
      this.demSource.setupMaplibre(maplibregl);
    }
  }
  teardown(map, maplibregl) {
    maplibregl.removeProtocol("pmtiles");
  }
  build() {
    return {
      id: "opentrailstash",
      version: 8,
      name: "OpenTrailStash",
      sources: {
        trailstash: {
          type: "vector",
          url: this.tileUrl,
          maxzoom: 15,
          minzoom: 0,
        },
        natural_earth_shaded_relief: {
          maxzoom: 6,
          tileSize: 256,
          tiles: [
            "https://trailstash.github.io/naturalearthtiles/tiles/natural_earth_2_shaded_relief.raster/{z}/{x}/{y}.png",
          ],
          type: "raster",
        },
        ...(this.demSource
          ? {
              terrarium: {
                type: "raster-dem",
                tiles: [this.demSource.sharedDemProtocolUrl],
                minzoom: 0,
                maxzoom: 15,
                tileSize: 256,
                encoding: "terrarium",
              },
              contours: {
                type: "vector",
                tiles: [
                  this.demSource.contourProtocolUrl({
                    // convert meters to feet, default=1 for meters
                    multiplier: 3.28084,
                    thresholds: {
                      // zoom: [minor, major]
                      11: [200, 1000],
                      12: [100, 500],
                      14: [50, 200],
                      15: [20, 100],
                    },
                    contourLayer: "contour",
                    elevationKey: "ele",
                    levelKey: "level",
                    extent: 4096,
                    buffer: 1,
                  }),
                ],
              },
            }
          : {}),
      },
      sprite: `${this.root}sprites/opentrailstash`,
      glyphs: `${this.root}fonts/{fontstack}/{range}.pbf`,
      layers: [
        {
          id: "background",
          type: "background",
          layout: {
            visibility: "visible",
          },
          paint: {
            "background-color": "rgb(239,239,239)",
          },
        },
        naturalEarthShadedReliefLayer,
        landuseLayers.park,
        landcoverLayers.green,
        landcoverLayers.wetland,
        landcoverLayers.glacier,
        // residential
        // industrial
        landuseLayers.cemetery,
        landuseLayers.golf,
        // hospital
        landuseLayers.school,
        // quarry,
        // stadium,
        // track(landuse),
        landuseLayers.pitch,
        landuseLayers.playground,
        landcoverLayers.sand,
        ...(this.demSource ? [hillshadeLayer] : []),
        ...(this.demSource ? contourLayers : []),
        ...waterLayers,
        landuseLayers.aerodrome,
        // wilderness/
        ...landuseLayers.parkOutline,
        ...tunnelTranspoLayers,
        ...surfaceTranspoLayers,
        ...transpoLabelLayers,
        buildingsLayer,
        ...boundariesLayers,
        ...waterLabelLayers,
        ...bridgeTranspoLayers,
        ...poiLayers.generic,
        poiLayers.airport,
        poiLayers.simple("station", "railway_11"),
        poiLayers.simple("supermarket", "grocery_15"),
        poiLayers.simple("convenience", "shop_15"),
        poiLayers.simple("bicycle_parking", "bicycle_parked_11"),
        poiLayers.simple("camp_pitch", "campsite_11"),
        poiLayers.simple("camp_site", "campsite_15"),
        poiLayers.simple("bicycle", "bicycle_15"),
        poiLayers.simple("bicycle_rental", "bicycle_rental_15"),
        poiLayers.simple("shelter", "shelter_15"),
        poiLayers.simple("toilets", "toilet_11"),
        poiLayers.simple("drinking_water", "drinking_water_15"),
        poiLayers.simple("bicycle_repair_station", "bicycle_repair_15"),
        physicalLayers.spring,
        physicalLayers.saddle,
        physicalLayers.peak,
        poiLayers.park,
        ...placesLayers,
      ],
    };
  }
}

export default TrailStash;
