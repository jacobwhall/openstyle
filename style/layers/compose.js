export default (...layers) =>
  layers.reduce(
    (acc, cur) =>
      cur instanceof Function
        ? cur(acc)
        : {
            ...acc,
            ...cur,
            paint: { ...acc.paint, ...cur.paint },
            layout: { ...acc.layout, ...cur.layout },
          },
    {}
  );
