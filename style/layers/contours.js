export const contourLayers = [
  {
    id: "contour_index",
    type: "line",
    source: "contours",
    "source-layer": "contour",
    filter: [">", ["get", "level"], 0],
    layout: {
      visibility: "visible",
    },
    paint: {
      "line-color": "hsl(22,35%,55%)",
      "line-width": 1.3,
      "line-opacity": {
        stops: [
          [7, 0.2],
          [10, 0.6],
        ],
      },
    },
  },
  {
    id: "contour",
    type: "line",
    source: "contours",
    "source-layer": "contour",
    layout: {
      visibility: "visible",
    },
    paint: {
      "line-color": "hsl(22,35%,55%)",
      "line-width": 0.8,
      "line-opacity": 0.5,
    },
  },
  {
    id: "contour_name",
    type: "symbol",
    metadata: {},
    source: "contours",
    "source-layer": "contour",
    filter: [">", ["get", "level"], 0],
    layout: {
      "text-font": ["Open Sans Regular"],
      "text-size": {
        base: 1,
        stops: [
          [15, 9.5],
          [20, 12],
        ],
      },
      "text-field": ["concat", ["number-format", ["get", "ele"], {}], "'"],
      visibility: "visible",
      "text-padding": 10,
      "symbol-placement": "line",
      "symbol-avoid-edges": true,
      "text-allow-overlap": false,
      "text-ignore-placement": false,
      "text-rotation-alignment": "map",
    },
    paint: {
      "text-color": "hsl(22,28%,35%)",
      "text-halo-blur": 1,
      "text-halo-color": "hsla(47,16%,89%,0.6)",
      "text-halo-width": 2,
    },
  },
];
