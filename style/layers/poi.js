const poi = {
  type: "symbol",
  source: "trailstash",
  "source-layer": "pois",
  layout: {
    "text-anchor": "top",
    "text-field": "{name}",
    "text-font": ["Open Sans Italic"],
    "text-max-width": 9,
    "text-offset": [0, 0.6],
    "text-size": 11,
    "icon-image": ["concat", ["get", "pmap:kind"], ["literal", "_11"]],
  },
  paint: {
    "text-color": "#666",
    "text-halo-blur": 0.5,
    "text-halo-color": "#ffffff",
    "text-halo-width": 1,
  },
};
export const park = {
  ...poi,
  id: "poi_park_big",
  filter: [
    "any",
    ["==", ["get", "pmap:kind"], "national_park"],
    ["==", ["get", "pmap:kind"], "nature_reserve"],
    ["==", ["get", "pmap:kind"], "protected_area"],
    ["==", ["get", "pmap:kind"], "forest"],
    ["==", ["get", "pmap:kind"], "park"],
  ],
  layout: {
    ...poi.layout,
    "icon-image": "park_11",
  },
};
export const airport = {
  ...poi,
  id: "poi_airport",
  filter: ["all", ["==", ["get", "pmap:kind"], "aerodrome"]],
  layout: {
    ...poi.layout,
    "icon-image": "airport_11",
  },
};
export const drinkingWater = {
  ...poi,
  id: "poi_drinking_water",
  filter: ["==", "pmap:kind", "drinking_water"],
};
export const generic = [
  {
    id: "poi_dot_z16",
    type: "circle",
    source: "trailstash",
    "source-layer": "pois",
    filter: [
      "all",
      ["!=", ["get", "pmap:kind"], "animal"],
      ["!=", ["get", "pmap:kind"], "attraction"],
      ["!=", ["get", "pmap:kind"], "tomb"],
    ],
    paint: {
      "circle-color": "#725a50",
      "circle-radius": 2,
    },
    //minzoom: 16,
  },
  {
    ...poi,
    id: "poi_z16",
    filter: [
      "all",
      ["!=", ["get", "pmap:kind"], "animal"],
      ["!=", ["get", "pmap:kind"], "attraction"],
      ["!=", ["get", "pmap:kind"], "tomb"],
    ],
    //minzoom: 16,
  },
];
export const simple = (kind, icon) => ({
  ...poi,
  id: `poi_${kind}`,
  filter: ["==", ["get", "pmap:kind"], kind],
  layout: {
    ...poi.layout,
    "icon-image": icon,
  },
});
