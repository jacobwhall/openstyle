export const green = {
  id: "natural_green",
  type: "fill",
  source: "trailstash",
  "source-layer": "natural",
  filter: [
    "any",
    [
      "in",
      "pmap:kind",
      "wood",
      "forest",
      "grass",
      "meadow",
      "wetland",
      "scrub",
    ],
  ],
  paint: {
    "fill-color": "hsl(100, 60%, 70%)",
    "fill-opacity": 0.3,
  },
};
export const wetland = {
  id: "landcover_wetland_pattern",
  type: "fill",
  source: "trailstash",
  "source-layer": "natural",
  filter: ["==", ["get", "pmap:kind"], "wetland"],
  paint: {
    "fill-antialias": false,
    "fill-opacity": 0.4,
    "fill-pattern": "pedestrian_polygon",
  },
};
export const glacier = {
  id: "natural_glacier",
  type: "fill",
  source: "trailstash",
  "source-layer": "natural",
  filter: ["==", "pmap:kind", "glacier"],
  paint: {
    "fill-color": "#e7e7e7",
  },
};
export const sand = {
  id: "natural_sand",
  type: "fill",
  source: "trailstash",
  "source-layer": "natural",
  filter: ["==", "pmap:kind", "sand"],
  paint: {
    "fill-color": "rgba(247, 239, 195, 1)",
  },
};
