import * as colors from "../colors.js";
import compose from "./compose.js";
import Values from "values.js";

const roundJoin = { "line-join": "round" };
const roundCap = { "line-cap": "round" };
const buttCap = { "line-cap": "butt" };
const roundCapAndJoin = { ...roundJoin, ...roundCap };

import { _case, or, and, eq, get, has, ne, not, le, ge } from "../filters.js";

const minZooms = {
  freeway: 6,
  highway: 7,
  major: 9,
  minor: 11,
  service: 13,
  track: 11,
  trail: 11,
  path: 11,
  steps: 13,
  cut: 14,
  sidewalk: 15,
  rail: 10,
};

export const layer = (name) => {
  if (name == "label") {
    return {
      id: `transpo_label`,
      source: "trailstash",
      "source-layer": "highway",
      minzoom: 6,
    };
  } else if (name == "private_label") {
    return {
      id: `transpo_private_label`,
      source: "trailstash",
      "source-layer": "highway",
      minzoom: 6,
      filter: eq(get("access"), "private"),
    };
  } else if (name == "rail") {
    return {
      id: `transpo_${name}`,
      source: "trailstash",
      "source-layer": "transit",
      filter: eq(get("pmap:kind"), "rail"),
      minzoom: minZooms[name],
    };
  }
  return {
    id: `transpo_${name}`,
    source: "trailstash",
    "source-layer": "highway",
    filter: eq(get("type"), name),
    minzoom: minZooms[name],
  };
};

const isTunnel = (layer) => ({
  ...layer,
  id: `tunnel_${layer.id}`,
  filter: and(le(get("layer"), -1), layer.filter),
});
const isSurfaceLevel = (layer) => ({
  ...layer,
  filter: or(
    and(eq(get("layer"), 0), layer.filter),
    and(not(has("layer")), layer.filter)
  ),
});
const isBridge = (layer) => ({
  ...layer,
  id: `bridge_${layer.id}`,
  filter: and(ge(get("layer"), 1), layer.filter),
});
const isLayer = (n) => (layer) => ({
  ...layer,
  id: `${layer.id}_${n}`,
  filter: and(eq(get("layer"), n), layer.filter),
});
const isLink = (layer) => ({
  ...layer,
  id: `${layer.id}_link`,
  filter: and(eq(get("link"), true), layer.filter),
});
const isNotLink = (layer) => ({
  ...layer,
  filter: and(ne(get("link"), true), layer.filter),
});
const isUnpaved = (layer) => ({
  ...layer,
  id: `${layer.id}_unpaved`,
  filter: and(eq(get("unpaved"), true), layer.filter),
});
const isThiccMinor = (layer) => ({
  ...layer,
  id: `${layer.id}_thicc`,
  filter: and(layer.filter, or(eq(get("unpaved"), true), has("cycleway"))),
});
const isNormalMinor = (layer) => ({
  ...layer,
  id: `${layer.id}`,
  filter: and(layer.filter, ne(get("unpaved"), true), not(has("cycleway"))),
});
const designatedAccess = eq(get("bike"), "designated");
const noAccess = eq(get("bike"), "no");
const hasCycletrack = (layer) => ({
  ...layer,
  id: `${layer.id}_cycletrack`,
  filter: and(eq(get("cycleway"), "cycletrack"), layer.filter),
});
const hasBikelane = (layer) => ({
  ...layer,
  id: `${layer.id}_bikelane`,
  filter: and(eq(get("cycleway"), "bikelane"), layer.filter),
});
const hasSharrows = (layer) => ({
  ...layer,
  id: `${layer.id}_sharrows`,
  filter: and(eq(get("cycleway"), "sharrows"), layer.filter),
});

////
//// Base styles
////
/// Main Lines
// Roads
const styleRoad =
  (width = 18) =>
  (layer) => ({
    ...layer,
    type: "line",
    id: `${layer.id}_road`,
    layout: { ...layer.layout, ...roundJoin },
    paint: {
      ...layer.paint,
      "line-color": colors.road,
      "line-width": {
        stops: [
          [layer.minzoom, 1],
          [20, width],
        ],
      },
    },
  });
// boost low zoom thiccness of minor unpaved roads & minor roads with bike infra
const styleModEnthiccen = (layer) => {
  const newLayer = structuredClone(layer);
  newLayer.paint["line-width"].stops[0][0] -= 1;
  return newLayer;
};
// "pathlike" main line - path, trail, steps
const stylePathlike = (layer) => ({
  ...layer,
  type: "line",
  layout: {},
  paint: {
    "line-width": {
      stops: [
        [0, 1.5],
        [11, 1.5],
        [20, 4],
      ],
    },
  },
});
const styleSidewalk = (layer) => ({
  ...layer,
  type: "line",
  layout: {},
  paint: {
    "line-width": {
      stops: [
        [15, 1.5],
        [20, 4],
      ],
    },
  },
});
// track main line
const styleTrack = (layer) => ({
  ...layer,
  type: "line",
  layout: { ...buttCap },
  paint: {
    "line-color": colors.trail,
    "line-width": {
      stops: [
        [layer.minzoom, 0.5],
        [20, 4],
      ],
    },
    "line-gap-width": {
      stops: [
        [layer.minzoom, 0.5],
        [20, 4],
      ],
    },
    "line-dasharray": [6, 3],
  },
});
// cuts
const styleCut = (layer) => ({
  ...layer,
  type: "line",
  layout: { ...roundCapAndJoin },
  paint: {
    "line-color": colors.cut,
    "line-width": {
      stops: [
        [12, 0.5],
        [16, 2],
      ],
    },
    "line-dasharray": [0.05, 2],
  },
});
// rail
const styleRail = (layer) => ({
  ...layer,
  type: "line",
  paint: {
    "line-color": "#888",
    "line-width": {
      stops: [
        [layer.minzoom, 0.4],
        [layer.minzoom + 1, 0.4],
        [20, 1],
      ],
    },
  },
});
const styleRailHatching = (layer) => ({
  ...layer,
  id: `${layer.id}_hatching`,
  type: "line",
  layout: buttCap,
  paint: {
    "line-color": "#888",

    "line-dasharray": [0.05, 3],
    "line-width": {
      stops: [
        [layer.minzoom, 4],
        [layer.minzoom + 1, 4],
        [20, 10],
      ],
    },
  },
});

/// Extra lines (casings, bridge/tunnel outlines)
// "pathlike" casing line - path, trail, steps, sidewalk
const stylePathlikeCasing = (layer) => ({
  ...layer,
  type: "line",
  id: `${layer.id}_casing`,
  layout: {},
  paint: {
    "line-color": layer.id.includes("sidewalk")
      ? colors.sidewalkCasing
      : colors.defaultAccessCasing,
    "line-width": {
      stops: [
        [layer.minzoom, 2],
        [20, 10],
      ],
    },
  },
});
// track casing line
const styleTrackCasing = (layer) => ({
  ...layer,
  type: "line",
  id: `${layer.id}_casing`,
  layout: {},
  paint: {
    ...layer.paint,
    "line-color": colors.defaultAccessCasing,
    "line-width": {
      base: 1.2,
      stops: [
        [layer.minzoom, 1.5],
        [20, 12],
      ],
    },
  },
});
// Extra casing/outline for pathlike layers for styling as bridge/tunnel (hide for surface level)
// requires use of styleModSurfaceLevel, styleModTunnel, styleModBridge
const stylePathlikeOutline = (layer) => ({
  ...layer,
  type: "line",
  id: `${layer.id}_outline`,
  layout: {},
  paint: {
    "line-color": colors.bridgeOutline,
    "line-width": {
      stops: [
        [layer.minzoom, 1],
        [20, 12],
      ],
    },
  },
});

////
//// Style modifications
////

/// "pathlike" mods - track, path, trail, steps, sidewalk
// add dash pattern for steps
const styleModStepsDash = {
  layout: buttCap,
  paint: { "line-dasharray": [0.5, 0.5] },
};
// add color & dash pattern for trails
const styleModTrail = (layer) => ({
  ...layer,
  layout: { ...layer.layout, ...roundJoin, ...buttCap },
  paint: {
    ...layer.paint,
    "line-color": colors.trail,
    "line-dasharray": [1.5, 1.5],
  },
});
// add color for paths/sidewalks/steps
const styleModPathColor = (layer) => ({
  ...layer,
  paint: {
    ...layer.paint,
    "line-color": layer.id.includes("sidewalk")
      ? colors.path
      : _case(
          designatedAccess,
          colors.designatedAccessCasing,
          //noAccess,
          //colors.noAccessCasing,
          colors.path
        ),
  },
});
// add color for non-moto casing (eg: track,trail,path,steps)
const styleModAccessCasingColor = (layer) => ({
  ...layer,
  paint: {
    ...layer.paint,
    "line-color": layer.id.includes("sidewalk")
      ? colors.sidewalkCasing
      : _case(
          designatedAccess,
          colors.path,
          //noAccess,
          //colors.noAccessCasing,
          colors.defaultAccessCasing
        ),
  },
});

/// common mods
// lighten the line color by an amount
const styleModLightenLine = (amount) => (layer) => {
  const newLayer = { ...layer };
  newLayer.paint = { ...layer.paint };

  if (typeof newLayer.paint["line-color"] == "string") {
    newLayer.paint["line-color"] = new Values(layer.paint["line-color"])
      .tint(amount)
      .hexString();
  }

  return newLayer;
};
// transform a road or pathlike layer by adding a dash array(unless present, eg steps) and color
const styleModUnpaved = (layer) => {
  const newLayer = { ...layer };

  newLayer.layout = { ...newLayer.layout, ...buttCap };

  newLayer.paint = {
    "line-dasharray": [2, 2],
    ...layer.paint,
    "line-color": layer.id.includes("steps")
      ? colors.trail
      : _case(designatedAccess, colors.unpavedCycleweay, colors.unpaved),
  };

  return newLayer;
};

/// Road mods
// add freeway color
const styleModRoadColor = (kind) => ({
  paint: {
    "line-color": colors[kind],
  },
});
// Transform a road layer into it's casing by widening & darkening it
const styleModRoadCasing = (layer) => {
  const newLayer = structuredClone(layer);
  newLayer.id = `${layer.id}_casing`;
  newLayer.type = "line";

  newLayer.paint["line-color"] = new Values(layer.paint["line-color"])
    .shade(40)
    .hexString();

  newLayer.paint["line-width"] = { ...layer.paint["line-width"] };
  newLayer.paint["line-width"].stops[0][1] =
    newLayer.paint["line-width"].stops[0][1] + 1;
  newLayer.paint["line-width"].stops[1][1] =
    newLayer.paint["line-width"].stops[1][1] + 2;

  return newLayer;
};
// Transform a road layer into it's link variant by making it narrower
const styleModLinkWidth = (layer) => {
  const newLayer = structuredClone(layer);
  newLayer.paint["line-width"].stops[0][1] =
    newLayer.paint["line-width"].stops[0][1] / 2;
  newLayer.paint["line-width"].stops[1][1] =
    newLayer.paint["line-width"].stops[1][1] / 2;
  return newLayer;
};
// Transform a road layer into bikeinfra by making it narrower, green
const styleModCycleway = (layer) => {
  const newLayer = structuredClone(layer);
  newLayer.layout = { ...roundCapAndJoin };
  newLayer.paint["line-color"] = colors.cycleway;
  newLayer.paint["line-width"] = {
    stops: [
      [0, 1.5],
      [11, 1.5],
      [20, 4],
    ],
  };
  return newLayer;
};
// transform cycleway infra into lane by adding dasharray
const styleModBikelane = (layer) => {
  const newLayer = structuredClone(layer);
  newLayer.paint["line-dasharray"] = [2, 2];
  return newLayer;
};
// transform cycleway infra into sharrow by adding dasharray
const styleModSharrows = (layer) => {
  const newLayer = structuredClone(layer);
  newLayer.paint["line-dasharray"] = [0.01, 2];
  return newLayer;
};

/// z-index (brunnel) mods
// Style surface roads & pathlikes. Add round cap & hide pathlike outlines
const styleModSurfaceLevel = (layer) => {
  const newLayer = { ...layer };
  newLayer.layout = { ...roundCap, ...newLayer.layout };
  if (layer.id.includes("outline")) {
    newLayer.layout = { ...newLayer.layout, visibility: "none" };
  }
  return newLayer;
};
// Style tunnels. Lighten & add dash array to roadCasing/pathlikeOutlines
const styleModTunnel = (layer) => {
  const newLayer = { ...layer };
  newLayer.paint = { ...layer.paint };
  if (layer.id.includes("outline")) {
    newLayer.paint["line-dasharray"] = [1, 0.5];
  }
  if (newLayer.id.includes("road") || layer.id.includes("outline")) {
    if (newLayer.id.includes("casing")) {
      newLayer.paint["line-dasharray"] = [1, 0.5];
    }
    newLayer.paint["line-width"] = { ...layer.paint["line-width"] };
    newLayer.paint["line-width"].stops = [
      ...newLayer.paint["line-width"].stops,
    ];
  }
  return newLayer;
};
// Style bridges. widen & darken road casings
const styleModBridge = (layer) => {
  const newLayer = { ...layer };
  newLayer.paint = { ...layer.paint };
  if (newLayer.id.includes("casing") && newLayer.id.includes("road")) {
    newLayer.paint["line-color"] = new Values(layer.paint["line-color"])
      .shade(30)
      .hexString();
    newLayer.paint["line-width"] = { ...layer.paint["line-width"] };
    newLayer.paint["line-width"].stops = [
      ...newLayer.paint["line-width"].stops,
    ];
    newLayer.paint["line-width"].stops[1][1] =
      newLayer.paint["line-width"].stops[1][1] + 1;
  }
  return newLayer;
};

const isNotDesignated = (layer) => ({
  ...layer,
  filter: and(layer.filter, not(designatedAccess)),
});
const isDesignated = (layer) => ({
  ...layer,
  id: `${layer.id}_designated`,
  filter: and(layer.filter, designatedAccess),
});

const roadOverlays = (layer) => [
  layer,
  compose(layer, hasCycletrack, styleModCycleway),
  compose(layer, hasBikelane, styleModCycleway, styleModBikelane),
  compose(layer, hasSharrows, styleModCycleway, styleModSharrows),
  compose(layer, isUnpaved, styleModUnpaved),
];

const transpoLayers = [
  compose(layer("sidewalk"), stylePathlikeOutline),
  compose(layer("path"), isDesignated, stylePathlikeOutline),
  compose(layer("steps"), stylePathlikeOutline),
  compose(
    layer("path"),
    isDesignated,
    stylePathlikeCasing,
    styleModAccessCasingColor
  ),
  compose(layer("path"), isNotDesignated, stylePathlikeOutline),
  compose(layer("trail"), stylePathlikeOutline),
  compose(layer("sidewalk"), stylePathlikeCasing),
  compose(layer("path"), isNotDesignated, stylePathlikeCasing),
  compose(layer("track"), styleTrackCasing),
  compose(layer("steps"), stylePathlikeCasing),
  compose(layer("trail"), stylePathlikeCasing),
  compose(
    layer("freeway"),
    isLink,
    styleRoad(18),
    styleModRoadColor("freeway"),
    styleModLinkWidth,
    styleModRoadCasing
  ),
  compose(layer("service"), styleRoad(6), styleModRoadCasing),
  compose(
    layer("minor"),
    styleRoad(8),
    isThiccMinor,
    styleModRoadCasing,
    styleModEnthiccen
  ),
  compose(layer("minor"), styleRoad(8), isNormalMinor, styleModRoadCasing),
  compose(
    layer("major"),
    styleRoad(12),
    styleModRoadColor("major"),
    styleModRoadCasing
  ),
  compose(
    layer("highway"),
    styleRoad(12),
    styleModRoadColor("highway"),
    styleModRoadCasing
  ),
  compose(
    layer("freeway"),
    isNotLink,
    styleRoad(18),
    styleModRoadColor("freeway"),
    styleModRoadCasing
  ),
  compose(layer("sidewalk"), styleSidewalk, styleModPathColor),
  compose(layer("path"), isNotDesignated, stylePathlike, styleModPathColor),
  compose(layer("cut"), styleCut),
  compose(
    layer("path"),
    isNotDesignated,
    isUnpaved,
    stylePathlike,
    styleModPathColor,
    styleModUnpaved
  ),
  compose(
    layer("freeway"),
    isLink,
    styleRoad(18),
    styleModRoadColor("freeway"),
    styleModLinkWidth
  ),
  ...roadOverlays(compose(layer("service"), styleRoad(6))),
  ...roadOverlays(
    compose(layer("minor"), isThiccMinor, styleRoad(8), styleModEnthiccen)
  ),
  ...roadOverlays(compose(layer("minor"), isNormalMinor, styleRoad(8))),
  ...roadOverlays(
    compose(layer("major"), styleRoad(12), styleModRoadColor("major"))
  ),
  ...roadOverlays(
    compose(layer("highway"), styleRoad(12), styleModRoadColor("highway"))
  ),
  compose(
    layer("freeway"),
    isNotLink,
    styleRoad(18),
    styleModRoadColor("freeway")
  ),
  compose(layer("path"), isDesignated, stylePathlike, styleModPathColor),
  compose(layer("steps"), stylePathlike, styleModPathColor, styleModStepsDash),
  compose(
    layer("path"),
    isDesignated,
    isUnpaved,
    stylePathlike,
    styleModUnpaved
  ),
  compose(
    layer("steps"),
    isUnpaved,
    stylePathlike,
    styleModPathColor,
    styleModUnpaved,
    styleModStepsDash
  ),
  compose(layer("track"), styleTrack),
  compose(layer("trail"), stylePathlike, styleModTrail),
  compose(layer("rail"), styleRail),
  compose(layer("rail"), styleRailHatching),
];
export const baseTunnelTranspoLayers = transpoLayers
  .map(isTunnel)
  .map(styleModTunnel)
  .map(styleModLightenLine(20));
export const baseBridgeTranspoLayers = transpoLayers
  .map(isBridge)
  .map(styleModBridge);
export const tunnelTranspoLayers = [
  ...baseTunnelTranspoLayers.map(isLayer(-5)),
  ...baseTunnelTranspoLayers.map(isLayer(-4)),
  ...baseTunnelTranspoLayers.map(isLayer(-3)),
  ...baseTunnelTranspoLayers.map(isLayer(-2)),
  ...baseTunnelTranspoLayers.map(isLayer(-1)),
];
export const bridgeTranspoLayers = [
  ...baseBridgeTranspoLayers.map(isLayer(1)),
  ...baseBridgeTranspoLayers.map(isLayer(2)),
  ...baseBridgeTranspoLayers.map(isLayer(3)),
  ...baseBridgeTranspoLayers.map(isLayer(4)),
  ...baseBridgeTranspoLayers.map(isLayer(5)),
];
export const surfaceTranspoLayers = [
  {
    id: "ferry",
    type: "line",
    source: "trailstash",
    "source-layer": "transit",
    filter: ["all", ["==", "route", "ferry"]],
    layout: {
      "line-cap": "round",
      "line-join": "round",
    },
    paint: {
      "line-color": "rgba(119, 121, 233, .5)",
      "line-width": {
        base: 1.2,
        stops: [
          [12, 1],
          [14, 3],
        ],
      },
      "line-dasharray": [1, 2],
    },
  },
  {
    id: "transit_runway",
    type: "line",
    source: "trailstash",
    "source-layer": "transit",
    filter: ["any", ["in", "pmap:kind_detail", "runway"]],
    paint: {
      "line-color": "#b2b5d1",
      "line-width": [
        "interpolate",
        ["exponential", 1.6],
        ["zoom"],
        11,
        4,
        18,
        30,
      ],
    },
  },
  {
    id: "transit_taxiway",
    type: "line",
    source: "trailstash",
    "source-layer": "transit",
    filter: ["any", ["in", "pmap:kind_detail", "taxiway"]],
    paint: {
      "line-color": "#b2b5d1",
      "line-width": [
        "interpolate",
        ["exponential", 1.6],
        ["zoom"],
        10,
        1,
        15,
        6,
      ],
    },
  },
  ...transpoLayers.map(isSurfaceLevel).map(styleModSurfaceLevel),
];
const styleLabelText = {
  type: "symbol",
  layout: {
    "text-field": "{name}",
    "text-font": ["Open Sans Regular"],
    "text-size": 12,
    "text-max-width": 10,
    "symbol-placement": "line",
    "symbol-spacing": 350,
    "text-padding": 5,
  },
  paint: {
    "text-color": colors.transpoLabel,
    "text-halo-color": colors.transpoLabelHalo,
    "text-halo-width": 1,
  },
};
const styleModPrivate = {
  layout: { "text-field": "private" },
  paint: { "text-color": colors.transpoPrivateLabel },
};
export const transpoLabelLayers = [
  compose(layer("label"), styleLabelText),
  compose(layer("private_label"), styleLabelText, styleModPrivate),
];
