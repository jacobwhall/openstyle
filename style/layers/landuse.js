import { green } from "./landcover.js";

export const park = {
  id: "park",
  type: "fill",
  source: "trailstash",
  "source-layer": "landuse",
  filter: [
    "any",
    ["==", ["get", "pmap:kind"], "national_park"],
    ["==", ["get", "pmap:kind"], "nature_reserve"],
    ["==", ["get", "pmap:kind"], "forest"],
    ["==", ["get", "pmap:kind"], "park"],
  ],
  paint: {
    "fill-color": "#d8e8c8",
    "fill-opacity": 0.7,
    "fill-outline-color": "rgba(95, 208, 100, 1)",
  },
};
export const parkOutline = [
  {
    ...park,
    id: "park_outline_glow",
    type: "line",
    paint: {
      "line-color": "rgba(95, 208, 100, .2)",
      "line-width": 2,
      "line-offset": 2,
    },
  },
  {
    ...park,
    id: "park_outline",
    type: "line",
    paint: {
      "line-color": "rgba(85, 198, 90, .3)",
      "line-width": 2,
    },
  },
];
export const pitch = {
  id: "landuse_pitch",
  type: "fill",
  source: "trailstash",
  "source-layer": "landuse",
  filter: ["all", ["==", "pmap:kind", "pitch"]],
  paint: {
    "fill-color": "hsla(98, 61%, 72%, 0.7)",
  },
};
export const school = {
  id: "landuse_school",
  type: "fill",
  source: "trailstash",
  "source-layer": "landuse",
  filter: ["all", ["==", "pmap:kind", "school"]],
  paint: {
    "fill-color": "rgb(236,238,204)",
  },
};
export const playground = {
  ...pitch,
  id: "landuse_playground",
  filter: ["all", ["==", "pmap:kind", "playground"]],
};
export const cemetery = {
  id: "landuse_cemetery",
  type: "fill",
  source: "trailstash",
  "source-layer": "landuse",
  filter: ["==", "pmap:Kind", "cemetery"],
  paint: {
    "fill-color": "hsl(75, 37%, 81%)",
  },
};

export const golf = {
  ...green,
  id: "golf",
  "source-layer": "landuse",
  filter: ["==", ["get", "pmap:kind"], "golf_course"],
};
export const aerodrome = {
  id: "aerodrome",
  type: "fill",
  source: "trailstash",
  "source-layer": "landuse",
  minzoom: 11,
  filter: ["==", ["get", "pmap:kind"], "aerodrome"],
  paint: {
    "fill-color": "rgba(229, 228, 224, 1)",
    "fill-opacity": 0.7,
  },
};
