////
//// Transportation colors
////
/// Roads
// Freeway
export const freeway = "hsl(26, 87%, 62%)";
export const highway = "hsl(34, 100%, 77%)";
export const major = "hsl(48, 100%, 83%)";
export const road = "white";
/// Paths, Trails, Cuts, Sidewalks, Steps, Bridges
export const trail = "rgb(70,70,70)";
export const cut = "rgba(65, 64, 64, 1)";
export const path = "white";
// activity type-based casing
export const defaultAccessCasing = "rgb(206, 172, 52)";
export const noAccessCasing = "rgb(219, 67, 62)";
export const designatedAccessCasing = "rgb(62,142,255)";
export const cycleway = "rgb(62,142,255)";
// sidewalk casing
export const sidewalkCasing = "rgb(200, 200, 200)";
// bridge outline
export const bridgeOutline = "black";
/// Misc
// Unpaved
export const unpaved = "rgb(150, 150, 150)";
export const unpavedCycleweay = "rgba(255,255,255,0.5)";

// Label
export const transpoLabel = "black";
// Label Halo
export const transpoLabelHalo = "white";
// Private Label
export const transpoPrivateLabel = "red";

////
//// Water
////
// Water
export const water = "#a0c8f0";
export const waterOutline = "rgb(153,190,235)";
// Label
export const waterLabel = "#5d60be";
// Label Halo
export const waterLabelHalo = "rgba(255,255,255,0.7)";

////
//// Places
////
// Water
// Label
export const placeLabel = "#555";
// Label Halo
export const placeLabelHalo = "rgba(255,255,255,0.7)";
