# OpenTrailStash [![BSD licensed](https://img.shields.io/badge/license-BSD-blue.svg)](https://github.com/maputnik/osm-liberty/blob/gh-pages/LICENSE.md)

<img align="right" height="128" alt="Icon of map with a magnifying glass" src="logo.svg" />
A free trail map made for unpaved bicycle exploration with world coverage using readily available
datasets that can also be self-hosted.

It consists of a [styleJS](./stylejs.md) style for [MapLibre
GL](https://maplibre.org/projects/maplibre-gl-js/) and a
[Planetiler](https://github.com/onthegomap/planetiler) Profile to
generate vector tiles.
<br>
<br>
<br>
<br>

**[View OpenTrailStash](https://open.trailsta.sh)**

## The OpenTrailStash OSM highway classification system

The main distinguising feature of OpenTrailStash is how `highway` features from OSM are
categorized.

The main principle behind the classification systme is that the OSM `highway` tags for
non-motorized use don't align with how a map should classify such features. Namely, OpenTrailStash
classifies them into two main categories: paths & trails.

## Fonts

Currently, the only fonts used are:

-   Open Sans Regular
-   Open Sans Semibold Italic
-   Open Sans Italic

The full ranges for each of these three font stacks are self-hosted. They were copied from the
[`openmaptiles-fonts`](https://github.com/kylebarron/openmaptiles-fonts) repository. These are
downloaded from [openmaptiles/fonts](https://github.com/openmaptiles/fonts/releases).

## Tile Layers

### Transportation

This is the most important layer of OpenTrailStash. It contains all roads, paths, trails, tracks,
etc. It is a custom schema implementing the TrailStash OSM highway classification system.

### Water

This layer is derived from [Protomaps Basemaps](https://github.com/protomaps/basemaps)'s water
layer, physical lines, and physical points layers. It simplifies the tiled data by omiting nearly
all fields except for `intermitent` and `name`.

### Buildings

This layer is derived from [Protomaps Basemaps](https://github.com/protomaps/basemaps)'s buildings
layers, but simplified to exclude all information for 3D rendering (eg: building parts, height).

### Protomaps Basemaps layers

The following layers are included from the [Protomaps
Basemaps](https://github.com/protomaps/basemaps) project without changes:

-   Natural
-   Places
-   Boundaries

## Sources

-   [OpenStreetMap](http://openstreetmap.org/) the main source of data
-   [Natural Earth](https://www.naturalearthdata.com/) for low-zoom water and place data
-   [Natural Earth Tiles](https://trailstash.github.io/naturalearthtiles/) for low-zoom relief shading
-   icons:
    -   [Maki](https://www.mapbox.com/maki-icons/)
        -   most ions
    -   [Temaki](https://github.com/ideditor/temaki)
        -   icons not in maki
        -   bicycle rental intea of bike from maki
    -   [NPS Symbol Library](https://www.nps.gov/maps/tools/symbol-library/index.html)
        -   deer used for WMAs
-   Hillshade layer, using public [AWS Terrain Tiles](https://registry.opendata.aws/terrain-tiles/)
-   Contours layer, using public [AWS Terrain Tiles](https://registry.opendata.aws/terrain-tiles/),
    powered by [MapLibre Contour](https://github.com/onthegomap/maplibre-contour).

## Map Design

The map design originates from OSM Liberty/LibertyTopo/Bright but strives to to emphasize unpaved
trails and roads by rendering them in a darker color and colors casing by bicycle access.

## Contributing

The style is developed using [charities](https://github.com/unvt/charites).

## Icon Design

A [Maki](https://github.com/mapbox/maki) icon set using colors to distinguish between icon categories.

**Color Palette**

| Color Name | Hex Value |
| ---------- | --------- |
| Blue       | `#5d60be` |
| Light Blue | `#4898ff` |
| Orange     | `#d97200` |
| Red        | `#ba3827` |
| Brown      | `#725a50` |
| Green      | `#76a723` |

**Modify Icons**

1. Take the `sprite-src/iconset/iconset-trailstash.json` and import it to the [Maki Editor](https://www.mapbox.com/maki-icons/editor/).
2. Apply your changes and download the icons in SVG format and the iconset in JSON format.
3. replace the contents of `sprite-src/iconset` with the contents of the zip download from the Maki
   Editor.
4. run `make`

**Note:** sprite zero seems to require node version 6.17.1 https://github.com/mapbox/spritezero/issues/84#issuecomment-656327476
