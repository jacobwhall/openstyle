package net.trailstash.open.layers;

import com.onthegomap.planetiler.FeatureCollector;
import com.onthegomap.planetiler.ForwardingProfile;
import com.onthegomap.planetiler.VectorTile;
import com.onthegomap.planetiler.reader.SourceFeature;
import com.protomaps.basemap.feature.FeatureId;
import com.protomaps.basemap.names.OsmNames;
import java.util.List;

public class Physical implements ForwardingProfile.FeatureProcessor, ForwardingProfile.FeaturePostProcessor {

  @Override
  public String name() {
    return "physical";
  }

  @Override
  public void processFeature(SourceFeature sf, FeatureCollector features) {
    if (sf.isPoint() && sf.hasTag("natural", "peak", "saddle", "spring")) {

      // TODO: rank based on ele

      String kind = "";

      int minZoom = 12;
      if (sf.hasTag("natural", "peak")) {
        kind = "peak";
        minZoom = 13;
      }
      if (sf.hasTag("natural", "saddle")) {
        kind = "saddle";
        minZoom = 13;
      }
      if (sf.hasTag("natural", "sprint")) {
        kind = "sprint";
        minZoom = 13;
      }

      var feat = features.point(this.name())
        .setId(FeatureId.create(sf))
        .setAttr("pmap:kind", kind)
        .setAttr("ele", sf.getString("ele"))
        .setZoomRange(minZoom, 15);

      // Server sort features so client label collisions are pre-sorted
      feat.setSortKey(minZoom);

      OsmNames.setOsmNames(feat, sf, 0);
    }
  }

  @Override
  public List<VectorTile.Feature> postProcess(int zoom, List<VectorTile.Feature> items) {
    return items;
  }
}
