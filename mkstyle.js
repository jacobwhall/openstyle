import { writeFileSync } from "fs";
import TrailStash from "./style/index.js";

const [root, tileUrl] = process.argv.slice(2);
const style = new TrailStash({ root, tileUrl });
writeFileSync("style.json", JSON.stringify(style.build()));
