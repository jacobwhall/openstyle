import MapView from "@trailstash/map-view";
import Americana from "@trailstash/stylejs-americana";
import "@trailstash/map-view/dist/index.css";

import TrailStash from "./style/index.js";

const root = window.location.origin + window.location.pathname;

const spritePath = `${root}americana-sprites/sprite`;
const americana = new Americana({ spritePath });

//const tileUrl = "http://localhost:8080/virginia.json";
const tileUrl = "https://tiles.trailstash.net/opentrailstash.json";

const style = new TrailStash({ root, tileUrl });

const view = new MapView({
  map: {
    container: "map",
    zoom: 14,
    center: [-77.46679, 37.53115],
    hash: true,
  },
  controls: [
    {
      position: "bottom-left",
      type: "LayerControl",
      options: {
        layers: [
          {
            name: "OpenTrailStash",
            style: style,
            icon: "./logo.png",
          },
          {
            name: "OSM OpenMapTiles",
            style: "https://overpass-ultra.trailsta.sh/style.json",
            icon: "./omt.png",
          },
          {
            name: "Americana",
            style: americana,
            icon: "./americana.png",
          },
        ],
      },
    },
    { type: "NavigationControl" },
    {
      type: "GeolocateControl",
      options: {
        positionOptions: {
          enableHighAccuracy: true,
        },
        trackUserLocation: true,
      },
    },
    {
      type: "FontAwesomeLinkControl",
      options: [
        "https://gitlab.com/trailstash/openstyle#opentrailstash",
        "About",
        "info",
      ],
    },
    { type: "EditOnOsmControl" },
    {
      type: "ScaleControl",
      options: {
        unit: "imperial",
      },
    },
  ],
  PersistView: true,
  InitializeViewFromIP: {
    IpInfoToken: "a6134fb16df81b",
  },
});

// register service worker for PWA
if ("serviceWorker" in navigator) {
  navigator.serviceWorker.register("./sw.js").then(function (registration) {
    registration.update();
    console.log("Service Worker Registered");
  });
}
